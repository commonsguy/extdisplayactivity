package com.commonsware.externaldisplayactivity

import android.app.ActivityOptions
import android.content.Intent
import android.hardware.display.DisplayManager
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)

    getSystemService(DisplayManager::class.java)
      .getDisplays(DisplayManager.DISPLAY_CATEGORY_PRESENTATION)
      .firstOrNull()
      ?.let { display ->
        Intent(this, ExternalDisplayActivity::class.java)
          .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_MULTIPLE_TASK)
          .let { intent ->
            ActivityOptions.makeBasic()
              .setLaunchDisplayId(display.displayId)
              .toBundle()
              .let { opts ->
                startActivity(intent, opts)
              }
          }
      }
  }
}
