package com.commonsware.externaldisplayactivity

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class ExternalDisplayActivity : AppCompatActivity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    setContentView(R.layout.activity_external)
    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
  }
}
